from setuptools import setup, find_namespace_packages

setup(
   name='auth',
   version='1.0',
   packages=find_namespace_packages(),
)
